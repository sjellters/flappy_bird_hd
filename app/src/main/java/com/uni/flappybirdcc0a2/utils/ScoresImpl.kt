package com.uni.flappybirdcc0a2.utils

import android.content.Context
import android.content.SharedPreferences

class ScoresImpl : Scores {

    companion object {
        private const val PREF_DEFAULT = "com.uni.flappybirdcc0a2.PREF_DEFAULT"
        private const val HIGH_SCORE = "high_score"
    }

    override fun getHighScore(context: Context): Int {
        val p: SharedPreferences = context.getSharedPreferences(
            PREF_DEFAULT,
            Context.MODE_PRIVATE
        )
        return p.getInt(HIGH_SCORE, 0)
    }

    override fun isNewBestScore(context: Context, score: Int): Boolean =
        score > getHighScore(context)

    override fun storeHighScore(context: Context, score: Int) {
        val p: SharedPreferences = context.getSharedPreferences(
            PREF_DEFAULT,
            Context.MODE_PRIVATE
        )
        p.edit().putInt(HIGH_SCORE, score).apply()
    }
}