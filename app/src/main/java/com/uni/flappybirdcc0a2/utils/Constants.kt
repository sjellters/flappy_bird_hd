package com.uni.flappybirdcc0a2.utils

object Constants {
    const val MS_PER_FRAME = 10
    const val GOLDEN_SCORE = 10
    const val UNDEFINED = -999f
}