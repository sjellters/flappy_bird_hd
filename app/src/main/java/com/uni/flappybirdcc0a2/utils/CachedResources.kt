package com.uni.flappybirdcc0a2.utils

import android.content.Context
import android.graphics.drawable.Drawable

interface CachedResources {
    companion object {
        const val BIRD_DOWN = 0
        const val BIRD_MID = 1
        const val BIRD_UP = 2
        const val PIPE_UP = 3
        const val PIPE_DOWN = 4
        const val COIN = 5
        const val PLAY_BUTTON = 6
        const val CARD_NEW = 7
        const val ZERO = 8
        const val ONE = 9
        const val TWO = 10
        const val THREE = 11
        const val FOUR = 12
        const val FIVE = 13
        const val SIX = 14
        const val SEVEN = 15
        const val EIGHT = 16
        const val NINE = 17
        const val GOLD_MEDAL = 18
        const val CLOUD_0 = 19
        const val CLOUD_1 = 20
        const val BG_GROUND_UP = 21
        const val BG_GROUND_DOWN = 22
        const val BG_GENERAL_DAY = 23
        const val BG_GENERAL_NIGHT = 24
        const val BG_SPLASH = 25
        const val BG_GAME_OVER = 26
        const val BG_FINAL_SCORE = 27
    }

    fun load(context: Context)
    fun getDrawable(index: Int): Drawable
    fun getDigit(digit: Int): Drawable
    fun getBirdDrawable(): Array<Drawable>
    fun getRandomCloud(): Drawable
}