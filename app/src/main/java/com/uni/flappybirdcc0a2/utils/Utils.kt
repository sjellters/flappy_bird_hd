package com.uni.flappybirdcc0a2.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.R
import kotlin.random.Random

object Utils {
    fun getSizeInPixels(context: Context, @DimenRes id: Int): Float =
        context.resources.getDimensionPixelSize(id).toFloat()

    fun getFloatSize(context: Context, @DimenRes id: Int): Float =
        context.resources.getDimension(id)

    fun getDrawable(context: Context, @DrawableRes id: Int): Drawable =
        ContextCompat.getDrawable(context, id)!!

    fun generateRandomInt(maxValue: Int): Int = Random.nextInt(0, maxValue)

    fun generateScore(context: Context, points: Int, cachedResources: CachedResources): Bitmap {
        val digits = points.toDigitsArray()
        var currentX = 0f

        val digitWidth = getSizeInPixels(context, R.dimen.score_digit_width)
        val height = getSizeInPixels(context, R.dimen.score_digit_height)
        val digitMargin = getSizeInPixels(context, R.dimen.score_digit_margin)

        val width = (digits.size * digitWidth) + (digitMargin * (digits.size - 1))

        val bitmap = Bitmap.createBitmap(width.toInt(), height.toInt(), Bitmap.Config.ARGB_8888)

        val canvas = Canvas(bitmap)

        digits.forEach { digit ->
            val drawable = cachedResources.getDigit(digit)
            drawable.bounds = RectF(currentX, 0f, currentX + digitWidth, height).toRect()
            drawable.draw(canvas)
            currentX += digitWidth + digitMargin
        }

        return bitmap
    }

    private fun Int.toDigitsArray(): Array<Int> {
        val digits = mutableListOf<Int>()
        var i = this

        if (i == 0) {
            digits.add(0)
        } else {
            while (i > 0) {
                digits.add(i % 10)
                i /= 10
            }
        }

        return digits.toTypedArray().reversedArray()
    }
}