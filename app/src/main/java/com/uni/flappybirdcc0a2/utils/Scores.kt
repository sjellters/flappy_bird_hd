package com.uni.flappybirdcc0a2.utils

import android.content.Context

interface Scores {
    fun getHighScore(context: Context): Int
    fun isNewBestScore(context: Context, score: Int): Boolean
    fun storeHighScore(context: Context, score: Int)
}