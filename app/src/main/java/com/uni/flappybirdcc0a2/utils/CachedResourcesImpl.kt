package com.uni.flappybirdcc0a2.utils

import android.content.Context
import android.graphics.drawable.Drawable
import com.uni.flappybirdcc0a2.R

class CachedResourcesImpl : CachedResources {

    private val drawables: Array<Drawable?> = arrayOfNulls(28)

    override fun load(context: Context) {
        val drawableResIds = intArrayOf(
            R.drawable.bird_down,
            R.drawable.bird_mid,
            R.drawable.bird_up,
            R.drawable.pipe_up,
            R.drawable.pipe_down,
            R.drawable.coin,
            R.drawable.play_button,
            R.drawable.card_new,
            R.drawable.zero,
            R.drawable.one,
            R.drawable.two,
            R.drawable.three,
            R.drawable.four,
            R.drawable.five,
            R.drawable.six,
            R.drawable.seven,
            R.drawable.eight,
            R.drawable.nine,
            R.drawable.gold_medal,
            R.drawable.cloud_0,
            R.drawable.cloud_1,
            R.drawable.bg_ground_up,
            R.drawable.bg_ground_down,
            R.drawable.bg_general_day,
            R.drawable.bg_general_night,
            R.drawable.bg_splash,
            R.drawable.bg_game_over,
            R.drawable.bg_final_score
        )
        drawableResIds.forEachIndexed { index, resId ->
            drawables[index] =
                Utils.getDrawable(
                    context,
                    resId
                )
        }
    }

    override fun getDrawable(index: Int): Drawable = drawables[index]!!

    override fun getDigit(digit: Int): Drawable = getDrawable(CachedResources.ZERO + digit)

    override fun getBirdDrawable(): Array<Drawable> = arrayOf(
        CachedResources.BIRD_DOWN,
        CachedResources.BIRD_MID,
        CachedResources.BIRD_UP
    ).map { getDrawable(it) }.toTypedArray()

    override fun getRandomCloud(): Drawable =
        getDrawable(CachedResources.CLOUD_0 + Utils.generateRandomInt(2))
}