package com.uni.flappybirdcc0a2

import android.annotation.SuppressLint
import android.graphics.Paint
import android.graphics.PixelFormat
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceHolder.Callback
import android.view.SurfaceView
import android.view.View
import android.view.View.OnTouchListener
import androidx.appcompat.app.AppCompatActivity
import com.uni.flappybirdcc0a2.databinding.ActivityMainBinding
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.utils.CachedResourcesImpl
import com.uni.flappybirdcc0a2.utils.Scores
import com.uni.flappybirdcc0a2.utils.ScoresImpl

class GameActivity : AppCompatActivity(), OnTouchListener, Callback {

    private lateinit var binding: ActivityMainBinding

    private lateinit var cachedResources: CachedResources
    private lateinit var scores: Scores
    private lateinit var gameProcessor: GameProcessor
    private lateinit var holder: SurfaceHolder
    private val globalPaint: Paint by lazy {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint
    }
    private var surfaceCreated: Boolean = false
    private var drawingThread: Thread? = null
    private lateinit var surfaceView: SurfaceView

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        cachedResources = CachedResourcesImpl()
        cachedResources.load(applicationContext)
        scores = ScoresImpl()

        surfaceView = binding.surfaceView

        surfaceView.keepScreenOn = true
        holder = surfaceView.holder

        surfaceView.setZOrderOnTop(true)
        surfaceView.setOnTouchListener(this)

        holder.addCallback(this)
        holder.setFormat(PixelFormat.TRANSLUCENT)

        gameProcessor = GameProcessor(
            applicationContext,
            holder,
            globalPaint,
            cachedResources,
            scores
        )
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_DOWN) {
            gameProcessor.onTap(event.x, event.y)
        }
        return false
    }

    override fun surfaceCreated(holder: SurfaceHolder) {
        surfaceCreated = true
        startDrawingThread()
    }

    override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) = Unit

    override fun surfaceDestroyed(holder: SurfaceHolder) {
        surfaceCreated = false
        stopDrawingThread()
    }

    private fun startDrawingThread() {
        stopDrawingThread()
        drawingThread = Thread {
            gameProcessor.execute()
        }
        drawingThread!!.start()
    }

    private fun stopDrawingThread() {
        drawingThread?.interrupt()
        try {
            drawingThread?.join()
        } catch (e: InterruptedException) {
            Log.e("GameActivity", "Failed to interrupt the drawing thread")
        }
        drawingThread = null
    }

    override fun onResume() {
        super.onResume()
        gameProcessor.resume()
    }

    override fun onPause() {
        gameProcessor.pause()
        super.onPause()
    }

}