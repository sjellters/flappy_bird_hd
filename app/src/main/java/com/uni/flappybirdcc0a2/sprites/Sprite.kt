package com.uni.flappybirdcc0a2.sprites

import android.graphics.Canvas
import android.graphics.Paint

interface Sprite {
    companion object {
        const val STATUS__PLAY = 0
        const val STATUS__NOT_STARTED = 1
        const val STATUS__GAME_OVER = 2
    }

    fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int)
    fun isAlive(): Boolean
    fun isCollided(sprite: Sprite): Boolean
    fun getScore(): Int
}