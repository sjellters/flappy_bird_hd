package com.uni.flappybirdcc0a2.sprites.cloud

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.utils.Constants.UNDEFINED
import com.uni.flappybirdcc0a2.utils.Utils

class CloudSprite(
    context: Context,
    cachedResources: CachedResources,
    private var x: Float
) : Sprite {

    private val drawable: Drawable = cachedResources.getRandomCloud()
    private var y: Float = UNDEFINED
    private var cloudWidth: Float = UNDEFINED
    private var cloudHeight: Float = UNDEFINED
    private val cloudMinHeight = Utils.getSizeInPixels(context, R.dimen.cloud_min_height)
    private val cloudMaxHeight = Utils.getSizeInPixels(context, R.dimen.cloud_max_height)
    private var speed: Float = UNDEFINED
    private var minSpeed: Float = Utils.getSizeInPixels(context, R.dimen.cloud_min_speed)
    private var maxSpeed: Float = Utils.getSizeInPixels(context, R.dimen.cloud_max_speed)
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        if (y == UNDEFINED) {
            val screenHeight = canvas.height
            y = Utils.generateRandomInt(screenHeight / 3).toFloat()
            // random speed
            speed = Utils.generateRandomInt((maxSpeed - minSpeed).toInt()) + minSpeed
            // random size
            cloudHeight =
                Utils.generateRandomInt((cloudMaxHeight - cloudMinHeight).toInt()) + cloudMinHeight
            cloudWidth = drawable.intrinsicWidth * cloudHeight / drawable.intrinsicHeight
        }
        isAlive = (status != Sprite.STATUS__NOT_STARTED && (x + cloudWidth > 0f))

        if (status == Sprite.STATUS__PLAY) {
            x -= speed
        }

        drawable.bounds = RectF(
            x,
            y,
            x + cloudWidth,
            y + cloudHeight
        ).toRect()
        drawable.draw(canvas)
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean = false

    override fun getScore(): Int = 0

}