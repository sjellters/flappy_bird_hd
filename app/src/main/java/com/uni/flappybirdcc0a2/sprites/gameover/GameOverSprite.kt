package com.uni.flappybirdcc0a2.sprites.gameover

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.sprites.splash.SplashInterface
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.utils.Constants
import com.uni.flappybirdcc0a2.utils.Utils

class GameOverSprite(
    private val context: Context,
    private val cachedResources: CachedResources,
    private val score: Int,
    private val newBestScore: Boolean,
    private val bestScore: Int,
    private var splashInterface: SplashInterface?
) : Sprite {

    private val titleDrawable: Drawable = cachedResources.getDrawable(CachedResources.BG_GAME_OVER)
    private val bgFinalScoreDrawable: Drawable =
        cachedResources.getDrawable(CachedResources.BG_FINAL_SCORE)
    private val newDrawable: Drawable = cachedResources.getDrawable(CachedResources.CARD_NEW)
    private val playDrawable: Drawable = cachedResources.getDrawable(CachedResources.PLAY_BUTTON)
    private val goldMedalDrawable: Drawable =
        cachedResources.getDrawable(CachedResources.GOLD_MEDAL)
    private val titleWidth: Float = Utils.getSizeInPixels(context, R.dimen.game_over_width)
    private val titleHeight: Float =
        titleDrawable.intrinsicHeight * titleWidth / titleDrawable.intrinsicWidth
    private val bgFinalScoreMarginTop: Float =
        Utils.getSizeInPixels(context, R.dimen.final_score_margin_top)
    private val bgFinalScoreMarginBottom: Float =
        Utils.getSizeInPixels(context, R.dimen.final_score_margin_bottom)
    private val bgFinalScoreWidth: Float = Utils.getSizeInPixels(context, R.dimen.final_score_width)
    private val bgFinalScoreHeight: Float =
        bgFinalScoreDrawable.intrinsicHeight * bgFinalScoreWidth / bgFinalScoreDrawable.intrinsicWidth
    private val newWidth: Float = Utils.getSizeInPixels(context, R.dimen.new_width)
    private val newHeight: Float =
        newDrawable.intrinsicHeight * newWidth / newDrawable.intrinsicWidth
    private val btnWidth: Float = Utils.getSizeInPixels(context, R.dimen.button_width)
    private val btnHeight: Float =
        btnWidth * playDrawable.intrinsicHeight / playDrawable.intrinsicWidth
    private val digitHeight: Float =
        Utils.getSizeInPixels(context, R.dimen.score_small_digit_height)
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        isAlive = status == Sprite.STATUS__GAME_OVER

        val screenWidth = canvas.width
        val screenHeight = canvas.height

        var bgSize =
            (screenHeight / 2f) - (titleHeight / 2f) - (bgFinalScoreHeight / 2f) - (bgFinalScoreMarginTop / 2f) - (bgFinalScoreMarginBottom / 2f)
        titleDrawable.bounds = RectF(
            screenWidth / 2f - titleWidth / 2f,
            bgSize - titleHeight / 2f,
            screenWidth / 2f + titleWidth / 2f,
            bgSize + titleHeight / 2f
        ).toRect()
        titleDrawable.draw(canvas)

        bgSize += titleHeight + bgFinalScoreMarginTop
        bgFinalScoreDrawable.bounds = RectF(
            screenWidth / 2f - bgFinalScoreWidth / 2f,
            bgSize,
            screenWidth / 2f + bgFinalScoreWidth / 2f,
            bgSize + bgFinalScoreHeight
        ).toRect()
        bgFinalScoreDrawable.draw(canvas)

        val scoreDeltaX =
            (screenWidth / 2f) + (bgFinalScoreWidth / 2f) - (bgFinalScoreWidth * .095f)
        val scoreBmp = Utils.generateScore(context, score, cachedResources)
        val scoreRatio = digitHeight / scoreBmp.height
        val scoreX = scoreDeltaX - (scoreBmp.width * scoreRatio)
        val scoreY = bgSize + bgFinalScoreHeight * .28f
        canvas.drawBitmap(
            scoreBmp,
            Rect(0, 0, scoreBmp.width, scoreBmp.height),
            RectF(
                scoreX,
                scoreY,
                scoreX + scoreBmp.width * scoreRatio,
                scoreY + scoreBmp.height * scoreRatio
            ),
            null
        )
        scoreBmp.recycle()

        val bestScoreBmp = Utils.generateScore(context, bestScore, cachedResources)
        val bestScoreRatio = digitHeight / bestScoreBmp.height
        val bestScoreX = scoreDeltaX - (bestScoreBmp.width * bestScoreRatio)
        val bestScoreY = bgSize + bgFinalScoreHeight * .65f
        canvas.drawBitmap(
            bestScoreBmp,
            Rect(0, 0, bestScoreBmp.width, bestScoreBmp.height),
            RectF(
                bestScoreX,
                bestScoreY,
                bestScoreX + bestScoreBmp.width * bestScoreRatio,
                bestScoreY + bestScoreBmp.height * bestScoreRatio
            ),
            null
        )
        bestScoreBmp.recycle()

        if (newBestScore) {
            val xNewBestScore =
                (screenWidth / 2f) + (bgFinalScoreWidth / 2f) - (bgFinalScoreWidth * .4f)
            val yNewBestScore = bgSize + bgFinalScoreHeight * .51f
            newDrawable.bounds = RectF(
                xNewBestScore,
                yNewBestScore,
                xNewBestScore + newWidth,
                yNewBestScore + newHeight
            ).toRect()
            newDrawable.draw(canvas)
        }

        if (score > Constants.GOLDEN_SCORE) {
            val xGoldMedal =
                (screenWidth / 2f) - (bgFinalScoreWidth / 2f) + (bgFinalScoreWidth * .11f)
            val yGoldMedal = bgSize + bgFinalScoreHeight * .37f
            val goldMedalSize = bgFinalScoreWidth * .20f
            goldMedalDrawable.bounds = RectF(
                xGoldMedal,
                yGoldMedal,
                xGoldMedal + goldMedalSize,
                yGoldMedal + goldMedalSize
            ).toRect()
            goldMedalDrawable.draw(canvas)
        }

        bgSize += bgFinalScoreHeight + bgFinalScoreMarginBottom
        playDrawable.bounds = RectF(
            screenWidth / 2f - btnWidth / 2f,
            bgSize,
            screenWidth / 2f + btnWidth / 2f,
            bgSize + btnHeight
        ).toRect()
        playDrawable.draw(canvas)
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean = false

    override fun getScore(): Int = 0

    fun onTap(x: Float, y: Float) {
        if (playDrawable.bounds.contains(x.toInt(), y.toInt())) {
            splashInterface?.onPlayButtonTapped()
        }
    }

}