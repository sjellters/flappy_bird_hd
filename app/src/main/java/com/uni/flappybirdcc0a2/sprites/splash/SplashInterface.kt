package com.uni.flappybirdcc0a2.sprites.splash

interface SplashInterface {
    fun onPlayButtonTapped()
}