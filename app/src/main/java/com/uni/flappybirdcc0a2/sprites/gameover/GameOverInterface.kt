package com.uni.flappybirdcc0a2.sprites.gameover

interface GameOverInterface {
    fun onReplayButtonTapped()
}