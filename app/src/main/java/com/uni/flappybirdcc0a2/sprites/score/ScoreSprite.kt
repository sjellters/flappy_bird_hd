package com.uni.flappybirdcc0a2.sprites.score

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.utils.Utils

class ScoreSprite(
    private val context: Context,
    private val cachedResources: CachedResources
) : Sprite {
    var currentScore: Int = 0
    private val marginTop: Float = Utils.getSizeInPixels(context, R.dimen.score_margin)
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        if (status == Sprite.STATUS__NOT_STARTED) {
            isAlive = false
            return
        }

        val scoreBitmap = Utils.generateScore(context, currentScore, cachedResources)
        val x = canvas.width / 2f - scoreBitmap.width / 2f
        val y = marginTop
        canvas.drawBitmap(scoreBitmap, x, y, null)
        scoreBitmap.recycle()
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean = false

    override fun getScore(): Int = 0
}