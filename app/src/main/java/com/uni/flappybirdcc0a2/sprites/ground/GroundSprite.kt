package com.uni.flappybirdcc0a2.sprites.ground

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.sprites.bird.BirdSprite
import com.uni.flappybirdcc0a2.utils.Utils

class GroundSprite(
    context: Context,
    cachedResources: CachedResources
) : Sprite {
    private val speed: Float = Utils.getSizeInPixels(context, R.dimen.sprite_speed)
    private var layerX: Float = 0f
    private var layerY: Float = 0f
    private val groundDown: Drawable = cachedResources.getDrawable(CachedResources.BG_GROUND_DOWN)
    private val groundUp: Drawable = cachedResources.getDrawable(CachedResources.BG_GROUND_UP)
    private val groundWidth: Float = Utils.getSizeInPixels(context, R.dimen.ground_width)
    private val groundHeight: Float = Utils.getSizeInPixels(context, R.dimen.ground_height)
    private val groupUpHeight: Float =
        groundHeight - Utils.getSizeInPixels(context, R.dimen.ground_margin)
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        isAlive = status != Sprite.STATUS__NOT_STARTED

        val screenWidth = canvas.width.toFloat()
        val screenHeight = canvas.height.toFloat()

        if (status == Sprite.STATUS__PLAY) {
            layerX -= speed
        }
        if (layerX < -groundWidth) {
            layerX = 0f
        }
        layerY = screenHeight - groundHeight

        groundDown.bounds = RectF(
            0f,
            layerY,
            screenWidth,
            screenHeight
        ).toRect()
        groundDown.draw(canvas)

        for (x in layerX.toInt() until screenWidth.toInt() step groundWidth.toInt()) {
            groundUp.bounds = RectF(
                x.toFloat(),
                layerY,
                x + groundWidth,
                layerY + groupUpHeight
            ).toRect()
            groundUp.draw(canvas)
        }
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean =
        (sprite is BirdSprite && sprite.getRect().bottom > layerY)

    override fun getScore(): Int = 0
}