package com.uni.flappybirdcc0a2.sprites.coin

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.sprites.bird.BirdSprite
import com.uni.flappybirdcc0a2.utils.Constants.UNDEFINED
import com.uni.flappybirdcc0a2.utils.Utils

class CoinSprite(
    context: Context,
    cachedResources: CachedResources,
    private var x: Float
) : Sprite {

    private val drawable: Drawable = cachedResources.getDrawable(CachedResources.COIN)
    private val coinWidth: Float = Utils.getSizeInPixels(context, R.dimen.coin_width)
    private val coinHeight: Float = Utils.getSizeInPixels(context, R.dimen.coin_width)
    private var y: Float = UNDEFINED
    private var speed: Float = Utils.getSizeInPixels(context, R.dimen.coin_speed)
    private var alpha: Int = 255
    private var alphaSpeed: Int = 3
    private val minAlpha = 128
    private val maxAlpha = 255
    private var isWon: Boolean = false
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        isAlive = (status != Sprite.STATUS__NOT_STARTED && !isWon && (x + coinWidth > 0f))
        if (y == UNDEFINED) {
            val screenHeight = canvas.height
            y = screenHeight / 4f + Utils.generateRandomInt(screenHeight / 2) - coinHeight / 2f
        }

        if (status == Sprite.STATUS__NOT_STARTED) {
            return
        }
        if (status == Sprite.STATUS__PLAY) {
            x -= speed
            // Animate the coin (flash effect).
            alpha -= alphaSpeed
            if (alpha < minAlpha) {
                alpha = minAlpha
                alphaSpeed = -alphaSpeed
            } else if (alpha > maxAlpha) {
                alpha = maxAlpha
                alphaSpeed = -alphaSpeed
            }
        }
        drawable.alpha = alpha
        drawable.bounds = getRect()
        drawable.draw(canvas)
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean = (isAlive() && sprite is BirdSprite
            && getRect().intersect(sprite.getRect())).also { isCollided ->
        if (isCollided) {
            isWon = true
        }
    }

    override fun getScore(): Int = if (isWon) 1 else 0

    private fun getRect(): Rect = RectF(
        x,
        y,
        x + coinWidth,
        y + coinHeight
    ).toRect()

}