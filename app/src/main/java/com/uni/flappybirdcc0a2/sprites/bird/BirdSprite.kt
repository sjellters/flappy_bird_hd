package com.uni.flappybirdcc0a2.sprites.bird

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.utils.Constants.UNDEFINED
import com.uni.flappybirdcc0a2.utils.Utils

class BirdSprite(
    context: Context,
    cachedResources: CachedResources,
) : Sprite {
    private var count: Int = 0
    private val drawables: Array<Drawable> = cachedResources.getBirdDrawable()
    private val birdHeight: Float = Utils.getSizeInPixels(context, R.dimen.bird_height)
    private val birdWidth: Float =
        birdHeight * drawables[0].intrinsicWidth / drawables[0].intrinsicHeight
    private val groundHeight: Float = Utils.getSizeInPixels(context, R.dimen.ground_height)
    private var x: Float = UNDEFINED
    private var y: Float = UNDEFINED
    private val acceleration: Float = Utils.getSizeInPixels(context, R.dimen.bird_acceleration)
    private var currentSpeed: Float = 0f
    private val tapSpeed: Float = Utils.getFloatSize(context, R.dimen.bird_tap_speed)
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        isAlive = status != Sprite.STATUS__NOT_STARTED

        val maxY = canvas.height - birdHeight - groundHeight
        val minY = 0f

        if (x == UNDEFINED && y == UNDEFINED) {
            x = canvas.width / 4 - birdWidth / 2 // 25%
            y = canvas.height / 2 - birdHeight / 2 // 50%
        }

        if (count >= 2) {
            count = 0
        }

        if (status != Sprite.STATUS__NOT_STARTED) {
            y += currentSpeed
            synchronized(this) {
                currentSpeed += acceleration
            }
        }

        if (y < minY) {
            y = minY
        } else if (y > maxY) {
            y = maxY
        }

        val birdDrawable: Drawable = if (status == Sprite.STATUS__PLAY) {
            drawables[count++]
        } else {
            drawables[0]
        }
        birdDrawable.bounds = getRect()
        birdDrawable.draw(canvas)
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean = false

    override fun getScore(): Int = 0

    fun getRect(): Rect = RectF(
        x,
        y,
        x + birdWidth,
        y + birdHeight
    ).toRect()

    fun jump() {
        synchronized(this) {
            currentSpeed = tapSpeed
            y -= currentSpeed
        }
    }
}