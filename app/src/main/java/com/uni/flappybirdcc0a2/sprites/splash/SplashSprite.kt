package com.uni.flappybirdcc0a2.sprites.splash

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.utils.Utils

class SplashSprite(
    context: Context,
    cachedResources: CachedResources,
    private var splashInterface: SplashInterface?
) : Sprite {
    private val hintDrawable: Drawable = cachedResources.getDrawable(CachedResources.BG_SPLASH)
    private val playDrawable: Drawable = cachedResources.getDrawable(CachedResources.PLAY_BUTTON)
    private val hintWidth: Float = Utils.getSizeInPixels(context, R.dimen.hint_width)
    private val hintHeight: Float =
        hintWidth * hintDrawable.intrinsicHeight / hintDrawable.intrinsicWidth
    private val playButtonWidth: Float = Utils.getSizeInPixels(context, R.dimen.button_width)
    private val getReadyMarginBottom: Float =
        Utils.getSizeInPixels(context, R.dimen.get_ready_margin_bottom)
    private val playButtonHeight: Float =
        playButtonWidth * playDrawable.intrinsicHeight / playDrawable.intrinsicWidth
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        isAlive = status == Sprite.STATUS__NOT_STARTED

        val screenWidth = canvas.width.toFloat()
        val screenHeight = canvas.height.toFloat()

        hintDrawable.bounds = RectF(
            screenWidth / 2f - hintWidth / 2f,
            screenHeight / 2f - hintHeight / 2f,
            screenWidth / 2f + hintWidth / 2f,
            screenHeight / 2f + hintHeight / 2f
        ).toRect()
        hintDrawable.draw(canvas)

        playDrawable.bounds = RectF(
            screenWidth / 2f - playButtonWidth / 2f,
            screenHeight / 2f + hintHeight / 2f + getReadyMarginBottom,
            screenWidth / 2f + playButtonWidth / 2f,
            screenHeight / 2f + hintHeight / 2f + getReadyMarginBottom + playButtonHeight
        ).toRect()
        playDrawable.draw(canvas)
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean = false

    override fun getScore(): Int = 0

    fun onTap(x: Float, y: Float) {
        if (playDrawable.bounds.contains(x.toInt(), y.toInt())) {
            splashInterface?.onPlayButtonTapped()
        }
    }

}