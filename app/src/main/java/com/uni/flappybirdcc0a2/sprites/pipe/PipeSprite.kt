package com.uni.flappybirdcc0a2.sprites.pipe

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRect
import com.uni.flappybirdcc0a2.R
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.sprites.bird.BirdSprite
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.utils.Constants.UNDEFINED
import com.uni.flappybirdcc0a2.utils.Utils
import kotlin.math.max
import kotlin.math.min

class PipeSprite(
    context: Context,
    cachedResources: CachedResources,
    var x: Float,
    val lastBlockY: Float?
) : Sprite {

    private val drawablePipeUp: Drawable = cachedResources.getDrawable(CachedResources.PIPE_UP)
    private val drawablePipeDown: Drawable = cachedResources.getDrawable(CachedResources.PIPE_DOWN)
    private val speed: Float = Utils.getSizeInPixels(context, R.dimen.sprite_speed)
    private val pipeWidth: Float = Utils.getSizeInPixels(context, R.dimen.pipe_width)
    private val gap: Float = Utils.getSizeInPixels(context, R.dimen.pipe_gap)
    private val swing: Float = Utils.getSizeInPixels(context, R.dimen.pipe_swing)
    private val groundHeight: Float = Utils.getSizeInPixels(context, R.dimen.ground_height)
    private val minUpHeight = Utils.getSizeInPixels(context, R.dimen.pipe_min)
    private var screenHeight: Float = 0f
    private var upHeight: Float = UNDEFINED
    private var scored: Boolean = false
    private var isAlive: Boolean = true

    override fun onDraw(canvas: Canvas, globalPaint: Paint, status: Int) {
        if (upHeight == UNDEFINED) {
            val screenHeight = canvas.height
            var min = minUpHeight + gap / 2f
            var max = screenHeight - min - groundHeight - (gap / 2f)
            if (lastBlockY != null) {
                min = max(min, lastBlockY - swing)
                max = min(max, lastBlockY + swing)
            }
            upHeight = Utils.generateRandomInt((max - min).toInt()) + min
        }
        isAlive = (status != Sprite.STATUS__NOT_STARTED && x + pipeWidth >= 0f)
        screenHeight = canvas.height.toFloat()

        if (status == Sprite.STATUS__NOT_STARTED) {
            return
        }
        if (status == Sprite.STATUS__PLAY) {
            x -= speed
        }
        drawablePipeUp.bounds = getTopPipeRect()
        drawablePipeUp.draw(canvas)
        drawablePipeDown.bounds = getBottomPipeRect()
        drawablePipeDown.draw(canvas)
    }

    override fun isAlive(): Boolean = isAlive

    override fun isCollided(sprite: Sprite): Boolean = (isAlive() && sprite is BirdSprite
            && ((getTopPipeRect().intersect(sprite.getRect()))
            || (getBottomPipeRect().intersect(sprite.getRect()))))

    override fun getScore(): Int = if (scored) 1 else 0

    private fun getTopPipeRect() = RectF(
        x,
        0f,
        x + pipeWidth,
        upHeight - gap / 2f
    ).toRect()

    private fun getBottomPipeRect() = RectF(
        x,
        upHeight + gap / 2f,
        x + pipeWidth,
        screenHeight - groundHeight
    ).toRect()

}