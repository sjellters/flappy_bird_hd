package com.uni.flappybirdcc0a2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.view.SurfaceHolder
import androidx.annotation.WorkerThread
import com.uni.flappybirdcc0a2.sprites.Sprite
import com.uni.flappybirdcc0a2.sprites.bird.BirdSprite
import com.uni.flappybirdcc0a2.sprites.cloud.CloudSprite
import com.uni.flappybirdcc0a2.sprites.coin.CoinSprite
import com.uni.flappybirdcc0a2.sprites.gameover.GameOverInterface
import com.uni.flappybirdcc0a2.sprites.gameover.GameOverSprite
import com.uni.flappybirdcc0a2.sprites.ground.GroundSprite
import com.uni.flappybirdcc0a2.sprites.pipe.PipeSprite
import com.uni.flappybirdcc0a2.sprites.score.ScoreSprite
import com.uni.flappybirdcc0a2.sprites.splash.SplashInterface
import com.uni.flappybirdcc0a2.sprites.splash.SplashSprite
import com.uni.flappybirdcc0a2.utils.CachedResources
import com.uni.flappybirdcc0a2.utils.Constants.MS_PER_FRAME
import com.uni.flappybirdcc0a2.utils.Scores
import com.uni.flappybirdcc0a2.utils.Utils

class GameProcessor(
    private val context: Context,
    private val holder: SurfaceHolder,
    private val globalPaint: Paint,
    private val cachedResources: CachedResources,
    private val scores: Scores,
) : SplashInterface, GameOverInterface {
    companion object {
        private const val MIN_PIPES = 10
    }

    private var currentStatus: Int = Sprite.STATUS__NOT_STARTED
    private var isPaused: Boolean = false
    private var points: Int = 0
    private var workSprites: MutableList<Sprite> = mutableListOf()
    private var birdSprite: BirdSprite? = null
    private var scoreSprite: ScoreSprite? = null
    private var groundSprite: GroundSprite? = null
    private var splashSprite: SplashSprite? = null
    private var gameOverSprite: GameOverSprite? = null
    private var lastPipeSprite: PipeSprite? = null
    private var countPipes: Int = 0
    private val pipeWidth = Utils.getSizeInPixels(context, R.dimen.pipe_width)
    private val coinWidth = Utils.getSizeInPixels(context, R.dimen.coin_width)
    private val pipeInterval = Utils.getSizeInPixels(context, R.dimen.pipe_interval)
    private var newBestScore: Boolean = false

    @WorkerThread
    fun execute() {
        while (!Thread.interrupted()) {
            if (isPaused) continue

            val startTime = System.currentTimeMillis()
            val canvas = holder.lockCanvas()
            val screenWidth = canvas.width.toFloat()

            try {
                cleanCanvas(canvas)

                val iterator: MutableListIterator<Sprite> = workSprites.listIterator()

                while (iterator.hasNext()) {
                    val sprite = iterator.next()

                    if (sprite.isAlive()) {
                        sprite.onDraw(canvas, globalPaint, currentStatus)
                    } else {
                        if (sprite is PipeSprite) {
                            scoreSprite?.currentScore = ++points
                            countPipes--
                        }

                        iterator.remove()
                    }
                }
            } finally {
                holder.unlockCanvasAndPost(canvas)
            }

            val duration = System.currentTimeMillis() - startTime
            val gap = MS_PER_FRAME - duration
            if (gap > 0) {
                try {
                    Thread.sleep(gap)
                } catch (e: Exception) {
                    break
                }
            }

            with(Sprite) {
                when (currentStatus) {
                    STATUS__NOT_STARTED -> {
                        if (splashSprite == null || !splashSprite!!.isAlive()) {
                            splashSprite =
                                SplashSprite(context, cachedResources, this@GameProcessor)
                            workSprites.add(splashSprite!!)
                        }
                    }
                    STATUS__GAME_OVER -> {
                        if (gameOverSprite == null || !gameOverSprite!!.isAlive()) {
                            gameOverSprite = GameOverSprite(
                                context,
                                cachedResources,
                                points,
                                newBestScore,
                                scores.getHighScore(context),
                                this@GameProcessor
                            )
                            workSprites.add(gameOverSprite!!)
                        }
                    }
                    STATUS__PLAY -> {
                        if (scoreSprite == null || !scoreSprite!!.isAlive()) {
                            scoreSprite = ScoreSprite(context, cachedResources)
                            workSprites.add(scoreSprite!!)
                        }

                        if (groundSprite == null || !groundSprite!!.isAlive()) {
                            groundSprite = GroundSprite(context, cachedResources)
                            workSprites.add(groundSprite!!)
                        }

                        if (birdSprite == null || !birdSprite!!.isAlive()) {
                            birdSprite = BirdSprite(context, cachedResources)
                            workSprites.add(birdSprite!!)
                        }

                        var nextPipeX = screenWidth
                        if (lastPipeSprite != null) {
                            nextPipeX = lastPipeSprite!!.x + pipeInterval
                        }

                        val cloudSprites = mutableListOf<CloudSprite>()
                        while (countPipes < MIN_PIPES) {
                            lastPipeSprite = PipeSprite(
                                context,
                                cachedResources,
                                nextPipeX,
                                lastPipeSprite?.lastBlockY
                            )
                            val lastCoinSprite = CoinSprite(
                                context,
                                cachedResources,
                                nextPipeX + pipeWidth + pipeInterval / 2f - coinWidth / 2f
                            )
                            val cloudSprite = CloudSprite(
                                context,
                                cachedResources,
                                nextPipeX
                            )

                            cloudSprites.add(cloudSprite)
                            workSprites.add(0, lastPipeSprite!!)
                            workSprites.add(0, lastCoinSprite)

                            nextPipeX += pipeWidth + pipeInterval
                            countPipes++
                        }

                        workSprites.addAll(0, cloudSprites)

                        val iterator: MutableListIterator<Sprite> = workSprites.listIterator()
                        while (iterator.hasNext()) {
                            val sprite = iterator.next()
                            if (sprite.isCollided(birdSprite!!)) {
                                when (sprite) {
                                    is PipeSprite, is GroundSprite -> {
                                        newBestScore = scores.isNewBestScore(context, points)
                                        if (newBestScore) {
                                            scores.storeHighScore(context, points)
                                        }
                                        currentStatus = Sprite.STATUS__GAME_OVER
                                    }
                                    is CoinSprite -> {
                                        points += sprite.getScore()
                                        scoreSprite?.currentScore = points
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    fun pause() {
        isPaused = true
    }

    fun resume() {
        isPaused = false
    }

    override fun onPlayButtonTapped() {
        startGame()
    }

    override fun onReplayButtonTapped() {
        startGame()
    }

    fun onTap(x: Float, y: Float) {
        when (currentStatus) {
            Sprite.STATUS__NOT_STARTED -> {
                splashSprite?.onTap(x, y)
            }
            Sprite.STATUS__PLAY -> {
                birdSprite?.jump()
            }
            Sprite.STATUS__GAME_OVER -> {
                gameOverSprite?.onTap(x, y)
            }
        }
    }

    private fun startGame() {
        resetGame()
        currentStatus = Sprite.STATUS__PLAY
    }

    private fun resetGame() {
        workSprites = mutableListOf()
        scoreSprite = null
        groundSprite = null
        birdSprite = null
        splashSprite = null
        gameOverSprite = null
        lastPipeSprite = null
        countPipes = 0
        points = 0
    }


    private fun cleanCanvas(canvas: Canvas) {
        canvas.drawColor(0x00000000, PorterDuff.Mode.CLEAR)
    }
}